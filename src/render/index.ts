// main script file to set listers with jQuery and route click events
// this import is this way to get the bundled version of Bootsrap incorporating popper.js
import "bootstrap/dist/js/bootstrap.bundle.min.js";
import * as $ from "jquery";

document.addEventListener("DOMContentLoaded", () => {

    // let test = new Popper();
    initListeners(); // add listers

});

function initListeners() {
    console.log('initListeners called');

    $("#add-button").click( () => {
        console.log($("#firstname-label").html)
        console.log($("#lastname-label").html)
    });
}