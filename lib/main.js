"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const electron_1 = require("electron");
const ElectronViewRenderer = require("electron-view-renderer");
let mainWindow;
let viewRenderer = configureViewRenderer();
function createWindow() {
    mainWindow = new electron_1.BrowserWindow({
        height: 600,
        width: 800,
    });
    viewRenderer.load(mainWindow, 'index', { myVar: "test" });
    mainWindow.on("closed", () => {
        mainWindow = null;
    });
}
function configureViewRenderer() {
    const viewRenderer = new ElectronViewRenderer({
        viewPath: 'src/view',
        viewProtcolName: 'view',
        useAssets: true,
        assetsPath: 'lib',
        assetsProtocolName: 'asset'
    });
    viewRenderer.use('ejs');
    return viewRenderer;
}
electron_1.app.on("ready", createWindow);
electron_1.app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        electron_1.app.quit();
    }
});
electron_1.app.on("activate", () => {
    if (mainWindow === null) {
        createWindow();
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uL3NyYy9tYWluLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsdUNBQThDO0FBRzlDLCtEQUE4RDtBQUU5RCxJQUFJLFVBQWtDLENBQUM7QUFDdkMsSUFBSSxZQUFZLEdBQUcscUJBQXFCLEVBQUUsQ0FBQTtBQUUxQztJQUVFLFVBQVUsR0FBRyxJQUFJLHdCQUFhLENBQUM7UUFDN0IsTUFBTSxFQUFFLEdBQUc7UUFDWCxLQUFLLEVBQUUsR0FBRztLQUNYLENBQUMsQ0FBQztJQUdILFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxFQUFDLEtBQUssRUFBRSxNQUFNLEVBQUMsQ0FBQyxDQUFBO0lBRXZELFVBQVUsQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRTtRQUkzQixVQUFVLEdBQUcsSUFBSSxDQUFDO0lBQ3BCLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQztBQUVEO0lBQ0UsTUFBTSxZQUFZLEdBQUcsSUFBSSxvQkFBb0IsQ0FBQztRQUM1QyxRQUFRLEVBQUUsVUFBVTtRQUNwQixlQUFlLEVBQUUsTUFBTTtRQUN2QixTQUFTLEVBQUUsSUFBSTtRQUNmLFVBQVUsRUFBRSxLQUFLO1FBQ2pCLGtCQUFrQixFQUFFLE9BQU87S0FDNUIsQ0FBQyxDQUFBO0lBQ0YsWUFBWSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtJQUV2QixNQUFNLENBQUMsWUFBWSxDQUFBO0FBQ3JCLENBQUM7QUFLRCxjQUFHLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxZQUFZLENBQUMsQ0FBQztBQUc5QixjQUFHLENBQUMsRUFBRSxDQUFDLG1CQUFtQixFQUFFLEdBQUcsRUFBRTtJQUcvQixFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDbEMsY0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2IsQ0FBQztBQUNILENBQUMsQ0FBQyxDQUFDO0FBRUgsY0FBRyxDQUFDLEVBQUUsQ0FBQyxVQUFVLEVBQUUsR0FBRyxFQUFFO0lBR3RCLEVBQUUsQ0FBQyxDQUFDLFVBQVUsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ3hCLFlBQVksRUFBRSxDQUFDO0lBQ2pCLENBQUM7QUFDSCxDQUFDLENBQUMsQ0FBQyJ9